<!--
SPDX-FileCopyrightText: 2024 Jordi Estrada

SPDX-License-Identifier: MIT
-->

# wbarchup

Waybar custom module to check for upgrades to installed packages on Arch Linux.

## Getting started

Install [uv](https://github.com/astral-sh/uv). Uv is a Python package installer and resolver:

```shell
# On Linux.
curl -LsSf https://astral.sh/uv/install.sh | sh

# With Pacman.
pacman -S uv
```

To create a virtual environment:

```shell
uv venv  # Create a virtual environment at .venv.
```

To activate the virtual environment:

```shell
# On bash.
source .venv/bin/activate

# On nushell.
overlay use .venv/bin/activate.nu
```

To install a package into the virtual environment:

```shell
uv pip install ./wbarchup-0.1.0-py3-none-any.whl  # Install wbarchup.
```

## Configuration

Run `wbarchup -h` for usage:

```
usage: wbarchup [-h] [-w WAIT] [-i INTERVAL]

Check for upgrades on installed packages

options:
  -h, --help            show this help message and exit
  -w WAIT, --wait WAIT  wait time in seconds. Default: 5
  -i INTERVAL, --interval INTERVAL
                        check interval in seconds. Default: 3600
```

Add to ~/.config/waybar/config.jsonc:

```json
"modules-right": ["custom/updates"],

"custom/updates": {
    "format": " {} {icon}",
    "return-type": "json",
    "format-icons": {
        "updates": "<b></b> ",
        "no-updates": "<b></b> "
    },
    "exec": "wbarchup"
```

Install Nerd Fonts or Font Awesome to display icons:

```shell
pacman -S otf-font-awesome
```

* License: MIT

# SPDX-FileCopyrightText: 2024 Jordi Estrada
#
# SPDX-License-Identifier: MIT

# Check for upgrades to installed packages on Arch Linux
# To be used within a waybar custom module

import argparse
import time
import subprocess
from contextlib import suppress


def format_out(text, tooltip):
    # The waybar module expects the script to output its data in json format like
    # this: {"text": "$text", "tooltip": "$tooltip", "class": "$class",
    #        "alt": "$alt"}
    if text > 0:
        out = f'{{"text": "{text}", "tooltip": "{tooltip}", "class": "updates", "alt": "updates"}}'
    else:
        out = f'{{"text": "{text}", "tooltip": "System updated", "class": "no-updates", "alt": "no-updates"}}'
    return out


def sync_database():
    # checkupdates --nocolor
    with suppress(subprocess.CalledProcessError, subprocess.TimeoutExpired):
        subprocess.run(
            ["checkupdates", "--nocolor"],
            timeout=60,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.STDOUT,
            text=True,
            check=True,
        )


def get_updates():
    # checkupdates --nosync --nocolor
    u = 0
    t = ""
    with suppress(subprocess.CalledProcessError):
        checkupdates = subprocess.run(
            ["checkupdates", "--nosync", "--nocolor"],
            capture_output=True,
            text=True,
            check=True,
        )
        if checkupdates.__dict__["stdout"] == "":
            u = 0
            t = ""
        else:
            t = checkupdates.__dict__["stdout"].replace("\n", "\\n")
            t = t[:-2]
            u = len(t.split("\\n"))
    return u, t


def main():
    # Parse the program arguments
    parser = argparse.ArgumentParser(
        description="Check for upgrades on installed packages"
    )
    parser.add_argument(
        "-w",
        "--wait",
        type=int,
        default=10,
        action="store",
        help="wait time in seconds. Default: 10",
    )
    parser.add_argument(
        "-i",
        "--interval",
        type=int,
        default=3600,
        action="store",
        help="check interval in seconds. Default: 3600",
    )
    args = parser.parse_args()

    wait = args.wait
    interval = args.interval
    iter = 0
    sync_database()

    try:
        update_on_iter = interval / wait
        while True:
            try:
                if iter >= update_on_iter:
                    # Check for upgrades from network
                    sync_database()
                    iter = 0
                # Check for upgrades from local database
                updates, tip = get_updates()
                print(format_out(updates, tip), flush=True)
                iter += 1
                time.sleep(wait)
            except KeyboardInterrupt:
                break

    except ZeroDivisionError:
        print("WAIT must be greater than 0")

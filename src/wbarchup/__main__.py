# SPDX-FileCopyrightText: 2024 Jordi Estrada
#
# SPDX-License-Identifier: MIT

import wbarchup
import sys

sys.exit(wbarchup.main())
